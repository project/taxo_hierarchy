<?php

/**
 * @file
 * Admin page callbacks for the taxo_hierarchy module.
 */

/**
 * Form builder to manage hierarchy settings for a vocabulary.
 *
 * @ingroup forms
 */
function taxo_hierarchy_form_vocabulary($form, &$form_state, $edit = array()) {
  $vocabulary = is_object($edit) ? $edit : (object) $edit;
  $form = array();

  // Check whether we want a confirmation form. (See
  // taxo_hierarchy_form_vocabulary_submit().)
  if (isset($form_state['confirm_clone'])) {
    // This form builder will check whether items are actually selected.
    $form = taxo_hierarchy_confirm_clone($form, $form_state, $vocabulary->vid);
  }
  if (!$form) {
    // Doublecheck / maybe change hierarchy, because of Core bug(s).
    taxo_hierarchy_check($vocabulary);

    $form['message'] = array(
      '#type' => 'markup',
      '#markup' => t('This vocabulary is of type: %type', array(
        '%type' => taxo_hierarchy_description($vocabulary->hierarchy)
      )),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    );

    if ($vocabulary->hierarchy != 2) {
      $form['message2'] = array(
        '#type' => 'markup',
        '#markup' => t("This form is empty, because it only has functionality to edit 'multiple hierarchy' type vocabularies."),
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      );
    }
    else {
      // Present a table with all multiple-parent terms, and offer an option to
      // 'clone' them (and give each cloned term 1 parent). taxonomy_get_tree()
      // loads terms with multiple parents multiple times. We only want to see
      // them once.
      $processed = array();
      // - I don't know how to get a lists of taxonomy objects, or entities,
      //   indexed by their IDs!
      // - taxonomy_term_load() returns entities without a 'parents' object.
      // Since we want to be able to look up a term _which has parents_ by its
      // ID, we'll have to generate such an array ourselves...
      $terms = array();
      foreach (taxonomy_get_tree($vocabulary->vid) as $term) {
        if (!isset($processed[$term->tid])) {
          $terms[$term->tid] = $term;
          $processed[$term->tid] = TRUE;
        }
      }

      // Build tablerow values.
      $m_terms = array();
      // Checkboxes cannot be 'named' in theme_tableselect, so we need to build
      // a separate key => tid storage.
      $processed = array();
      $row_id = 0;

      foreach ($terms as $term) {
        if (count($term->parents) > 1) {

          // Construct string for parents.
          $descriptions = array();
          foreach ($term->parents as $pid) {
            // Construct string for one parent:
            // all names until you reach a 'multiple' or 0.
            $parts = array($terms[$pid]->name);
            while (count($terms[$pid]->parents) == 1 && $terms[$pid]->parents[0]) {
              $pid = $terms[$pid]->parents[0];
              array_unshift($parts, $terms[$pid]->name);
            }
            if (count($terms[$pid]->parents) > 1) {
              array_unshift($parts, '&lt;multiple parents&gt;');
            }
            // Implode all terms into one 'path string'.
            $descriptions[] = implode(' &gt; ', $parts);
          }

          // Get all 'path strings' into one table cell.
          $m_terms[] = array(
            'id' => $term->tid,
            'name' => $term->name,
            'parents' => implode('<br/>', $descriptions),
          );
          $processed[$term->tid] = $row_id++;
        }
      }

      if ($m_terms) {
        $form['terms'] = array(
          '#type' => 'tableselect',
          '#header' => array(
            'id' => 'ID',
            'name' => 'Name',
            'parents' => 'Parents',
          ),
          '#options' => $m_terms,
        );
        $form['rows_by_term'] = array(
          '#type' => 'value',
          '#value' => $processed,
        );

        $form['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Clone terms'),
        );
        $form['submit-text'] = array(
          '#type' => 'markup',
          '#markup' => t('Cloning means that copies of the current terms will be made; the current term will keep one parent and each copy will have one other parent.'),
        );
      }
      else {
        // Message adds to the above 'this is a multiple-hierarchy vocabulary'.
        // It's unlikely that this message gets triggered, since Drupal resets
        // the type to 'single'.
        $form['message2'] = array(
          '#type' => 'markup',
          '#markup' => t('It does not actually contain terms with multiple parents, which is why this form is empty.'),
          '#prefix' => '<p>',
          '#suffix' => '</p>',
        );
      }
    }
  }
  return $form;
}

/**
 * Form submission handler for taxo_hierarchy_form_vocabulary().
 */
function taxo_hierarchy_form_vocabulary_submit($form, &$form_state) {
  if ($form_state['triggering_element']['#value'] == t('Clone terms')) {
    // Rebuild the form to confirm term cloning; see
    // taxo_hierarchy_confirm_clone().
    $form_state['rebuild'] = TRUE;
    $form_state['confirm_clone'] = TRUE;
  }
  // There is no other submit button on our form, so no more code here.
}

/**
 * Form builder for the term clone confirmation form.
 *
 * This form is on the same path as taxo_hierarchy_form_vocabulary(). This way
 * we don't have a separate URL with separate form builder for this function,
 * and after submit of this form our regular form will be rebuilt.
 *
 * @ingroup forms
 */
function taxo_hierarchy_confirm_clone($form, &$form_state, $vid) {
  $return_form = array();

  // Get checked row numbers.
  // Rather than messing with a function definition for array_filter, we process
  // the 0 row separately, which has value 0 for unchecked and "0" for checked.
  $checked_rows =
    ($form_state['values']['terms'][0] === "0" ? array(0) : array())
    + array_filter($form_state['values']['terms']);

  // Get Term IDs for all checked rows.
  if ($checked_rows) {
    $form['vid'] = array('#type' => 'value', '#value' => $vid);

    // Filter 'all row numbers, keyed by term ID' to contain only checked rows.
    $form['term_ids'] = array(
      '#type'  => 'value',
      '#value' => array_keys(
        array_intersect($form_state['values']['rows_by_term'], $checked_rows)
      ),
    );

    // This form builder was not called directly, so our submit function is not
    // the default. Set it explicitly.
    $form['#submit'] = array('taxo_hierarchy_confirm_clone_submit');

    $vocabulary = taxonomy_vocabulary_load($vid);
    $return_form = confirm_form($form,
      format_plural(count($form['term_ids']['#value']),
        'Are you sure you want to clone 1 term with multiple parents in %title?',
        'Are you sure you want to clone @count terms with multiple parents in %title?',
        array('%title' => $vocabulary->name)),
      "admin/structure/taxonomy/$vocabulary->machine_name/taxo_hierarchy",
      t('This action cannot be undone.'),
      t('Clone terms'),
      t('Cancel'));
  }
  else {
    // Only set a message. The caller will build the regular form for this path.
    drupal_set_message(t('No terms were selected!'), 'warning');
  }

  return $return_form;
}

/**
 * Submit handler to clone terms after confirmation.
 *
 * @todo batch processing (needed if there are a lot of terms).
 */
function taxo_hierarchy_confirm_clone_submit($form, &$form_state) {
  // Clone and 'un-parent' all selected terms. Do not redirect; the original
  // 'hierarchy admin' form will reload.
  if (empty($form_state['values']['vid'])) {
    drupal_set_message(t('Vocabulary is not specified!'), 'error');
  }
  elseif (empty($form_state['values']['term_ids'])) {
    drupal_set_message(t('Terms to clone are not specified!'), 'error');
  }
  else {
    $updated = 0;
    $new = 0;
    $skipped = 0;
    $terms = taxonomy_term_load_multiple($form_state['values']['term_ids']);
    foreach ($terms as $term) {
      if ($term->vid != $form_state['values']['vid']) {
        drupal_set_message(t('Vocabulary for term @t is not equal to specified vocabulary! This should not be possible; aborting further processing!'), 'error');
        break;
      }

      $first_parent_tid = 0;
      $tid = $term->tid;
      $parents = taxonomy_get_parents($tid);
      if (count($parents) < 2) {
        // Somehow, at this moment the term does not have multiple parents
        // anymore.
        $skipped++;
      }
      else {
        foreach ($parents as $parent) {
          if (!$first_parent_tid) {
            // Keep the first parent for last, to save with the original term.
            $first_parent_tid = $parent->tid;
          }
          else {
            // Save the term as a new one, with different parent.
            $term->parent = $parent->tid;
            unset($term->tid);
            taxonomy_term_save($term);
            $new++;
          }
        }
        // Save original term again, with first parent.
        $term->parent = $first_parent_tid;
        $term->tid = $tid;
        taxonomy_term_save($term);
        $updated++;
      }
    }
    $msg = format_plural($updated, '1 term was updated to have one parent',
        '@count terms were updated to have one parent') . '; ' .
           format_plural($new, '1 new term was created for the other parents.',
             '@count new terms were created for the other parents.');
    if ($skipped) {
      $msg .= ' ' . format_plural($skipped,
          "1 term was skipped because it doesn't have multiple parents (anymore?)",
          "@count terms were skipped because they don't have multiple parents (anymore?)");
    }
    drupal_set_message($msg);

    // Set the hierarchy back to 'single' if there are no multi-parent terms.
    taxo_hierarchy_check($form_state['values']['vid']);
  }
}

/**
 * Replacement for taxonomy_check_vocabulary_hierarchy().
 *
 * taxonomy_check_vocabulary_hierarchy has a bug ("&& !isset($term->parents[0]"
 * is weird) that makes the hierarchy "flat". Below code is from issue #1925638.
 *
 * @param int|object $vocabulary
 *   A vocabulary object or vid. In the first case, can be changed.
 */
function taxo_hierarchy_check($vocabulary) {
  if (!is_object($vocabulary)) {
    $vocabulary = taxonomy_vocabulary_load($vocabulary);
  }

  // Check if any term has a parent.
  $query = db_select('taxonomy_term_data', 't');
  $query->join('taxonomy_term_hierarchy', 'h', 'h.tid = t.tid');
  $has_parents = $query
    ->fields('t', array('vid'))
    ->condition('t.vid', $vocabulary->vid)
    ->condition('h.parent', 0, '>')
    ->range(0, 1)
    ->execute()
    ->fetchField();
  $hierarchy = $has_parents ? 1 : 0;

  // Only check multiple parents if there are any parents at all.
  if ($hierarchy == 1) {
    // Check if any term has more than one parent.
    $query = db_select('taxonomy_term_data', 't');
    $query->join('taxonomy_term_hierarchy', 'h1', 'h1.tid = t.tid');
    $query->join('taxonomy_term_hierarchy', 'h2', 'h2.tid = h1.tid AND h2.parent <> h1.parent');
    $multiple_parents = $query
      ->fields('t', array('vid'))
      ->condition('t.vid', $vocabulary->vid)
      ->range(0, 1)
      ->execute()
      ->fetchField();
    $hierarchy = $multiple_parents ? 2 : 1;
  }

  if ($hierarchy != $vocabulary->hierarchy) {
    $old = $vocabulary->hierarchy;
    $vocabulary->hierarchy = $hierarchy;
    taxonomy_vocabulary_save($vocabulary);
    drupal_set_message(t("Hierarchy type of %v has been changed from '@old' to '@new'.",
      array(
        '%v'   => $vocabulary->name,
        '@old' => taxo_hierarchy_description($old),
        '@new' => taxo_hierarchy_description($vocabulary->hierarchy),
      )));
  }
}

/**
 * Return descriptive string for a taxonomy hierarchy type.
 *
 * @param int $hierarchy
 *   Hierarchy 'type ID'
 *
 * @return string
 *   Translated description belonging to the ID
 */
function taxo_hierarchy_description($hierarchy) {
  switch ($hierarchy) {
    case 0:
      return t('flat');
    case 1:
      return t('single hierarchy');
    case 2:
      return t('multiple hierarchy');
    default:
      return t('unknown');
  }
}
